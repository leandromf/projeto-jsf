--
-- Dumping data for table cidade
--

INSERT INTO cidade (IdCidade,Nome) VALUES 
 (1,'PATOS DE MINAS'),
 (3,'PATROCÍNIO'),
 (5,'UBERLÂNDIA'),
 (6,'UBERABA'),
 (13,'ARAGUARI'),
 (20,'GOIÂNIA'),
 (21,'BRASÍLIA'),
 (22,'FORTALEZA'),
 (24,'JOÃO PESSOA'),
 (25,'CAMAÇARI'),
 (26,'ANARQUILÓPOLIS');

--
-- Dumping data for table endereco
--

INSERT INTO endereco (IdEndereco,Bairro,CEP,Complemento,NomeLogradouro,Numero,IdCidade,IdEstado,IdPessoa,IdTipoEndereco,IdTipoLogradouro) VALUES 
 (1,'AHFGSDGFS','11111-111',0,'WERTYUI',21,1,11,1,1,1);

--
-- Dumping data for table estado
--
INSERT INTO estado (IdEstado,NomeEstado) VALUES 
 (1,'ACRE'),
 (2,'ALAGOAS'),
 (3,'AMAZONAS'),
 (4,'AMAPÁ'),
 (5,'BAHIA'),
 (6,'CEARÁ'),
 (7,'DISTRITO FEDERAL'),
 (8,'ESPÍRITO SANTO'),
 (9,'GOIÁS'),
 (10,'MARANHÃO'),
 (11,'MINAS GERAIS'),
 (12,'MATO GROSSO DO SUL'),
 (13,'MATO GROSSO'),
 (14,'PARÁ'),
 (15,'PARAÍBA'),
 (16,'PERNAMBUCO'),
 (17,'PIAUÍ'),
 (18,'PARANÁ'),
 (19,'RIO DE JANEIRO'),
 (20,'RIO GRANDE DO NORTE'),
 (21,'RORAIMA'),
 (22,'RONDÔNIA'),
 (23,'RIO GRANDE DO SUL'),
 (24,'SANTA CATARINA'),
 (25,'SERGIPE'),
 (26,'SÃO PAULO'),
 (27,'TOCANTINS');

--
-- Dumping data for table pessoa
--

INSERT INTO pessoa (IdPessoa,CPF,DataDeCadastro,DataDeNascimento,Email,Name,Telefone,IdSexo,Login,Permissao,Senha) VALUES 
 (1,'12345667644','2013-01-12','2013-01-01','teste@live.com','ADMIN','(11) 1111-1111',1,'admin','ROLE_ADMIN','d033e22ae348aeb5660fc2140aec35850c4da997');

--
-- Dumping data for table sexo
--

INSERT INTO sexo (IdSexo,Descricao) VALUES 
 (2,'FEMININO'),
 (1,'MASCULINO');

--
-- Dumping data for table tipoendereco
--

INSERT INTO tipoendereco (IdTipoEndereco,DescricaoTipoEndereco) VALUES 
 (1,'RESIDENCIAL'),
 (2,'COMERCIAL'),
 (3,'RODOVIA'),
 (4,'FAZENDA'),
 (5,'INDÚSTRIA'),
 (6,'CHÁCARA'),
 (7,'VILA');

--
-- Dumping data for table tipologradouro
--

INSERT INTO tipologradouro (IdTipoLogradouro,DescricaoTipoLogradouro) VALUES 
 (1,'RUA'),
 (2,'ALAMEDA'),
 (3,'AVENIDA'),
 (4,'VIELA'),
 (5,'BECO'),
 (6,'TRAVESSA'),
 (7,'PRAÇA');