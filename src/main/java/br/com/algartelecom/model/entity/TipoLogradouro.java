package br.com.algartelecom.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "tipologradouro")
public class TipoLogradouro implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "IdTipoLogradouro", nullable = false)
	private Integer idTipoLogradouro;
	@Column(name = "DescricaoTipoLogradouro", nullable = false, length = 40)
	private String descricaoTipoLogradouro;

	@OneToMany(mappedBy = "tipoLogradouro", fetch = FetchType.LAZY)
	@ForeignKey(name = "EnderecoTipoLogradouro")
	private List<Endereco> enderecos;

	public TipoLogradouro() {

	}

	public Integer getIdTipoLogradouro() {
		return idTipoLogradouro;
	}

	public String getDescricaoTipoLogradouro() {
		return descricaoTipoLogradouro;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setIdTipoLogradouro(Integer idTipoLogradouro) {
		this.idTipoLogradouro = idTipoLogradouro;
	}

	public void setDescricaoTipoLogradouro(String descricaoTipoLogradouro) {
		this.descricaoTipoLogradouro = descricaoTipoLogradouro;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTipoLogradouro == null) ? 0 : idTipoLogradouro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoLogradouro other = (TipoLogradouro) obj;
		if (idTipoLogradouro == null) {
			if (other.idTipoLogradouro != null)
				return false;
		} else if (!idTipoLogradouro.equals(other.idTipoLogradouro))
			return false;
		return true;
	}

}
