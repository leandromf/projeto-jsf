package br.com.algartelecom.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "pessoa")
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "IdPessoa", nullable = false)
	private Integer idPessoa;
	@Column(name = "Nome", nullable = false, length = 80)
	private String nome;
	@Column(name = "Email", nullable = false, length = 80)
	private String email;
	@Column(name = "Telefone", nullable = false, length = 15)
	private String telefone;
	@Column(name = "CPF", nullable = false, length = 14)
	private String cpf;
	@Column(name = "DataDeNascimento", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataDeNascimento;
	@Column(name = "DataDeCadastro", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataDeCadastro;

	@Column(name = "Login", unique = true, length = 25)
	private String login;
	@Column(name = "Senha", length = 40)
	private String senha;
	@Column(name = "Permissao", length = 36)
	private String permissao;

	@OneToOne(mappedBy = "pessoa", fetch = FetchType.LAZY)
	@ForeignKey(name = "EnderecoPessoa")
	private Endereco endereco;

	@ManyToOne(optional = false)
	@ForeignKey(name = "PessoaSexo")
	@JoinColumn(name = "IdSexo", referencedColumnName = "IdSexo")
	private Sexo sexo;

	public Pessoa() {
		this.sexo = new Sexo();
	}

	public Integer getIdPessoa() {
		return idPessoa;
	}

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public Date getDataDeCadastro() {
		return dataDeCadastro;
	}

	public String getLogin() {
		return login;
	}

	public String getSenha() {
		return senha;
	}

	public String getPermissao() {
		return permissao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public void setDataDeCadastro(Date dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setPermissao(String permissao) {
		this.permissao = permissao;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPessoa == null) ? 0 : idPessoa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (idPessoa == null) {
			if (other.idPessoa != null)
				return false;
		} else if (!idPessoa.equals(other.idPessoa))
			return false;
		return true;
	}

}
