package br.com.algartelecom.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

	public static final String HIBERNATE_SESSION = "hibernate_session";
	private static final SessionFactory sessionFactory;

	static {
		try {
			System.out.println("Tentando iniciar session factory");
			Configuration configuration = new Configuration().configure();
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
					.buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			System.out.println("Session factory criada corretamente");
		} catch (Exception e) {
			System.out.println("Ocorreu um erro ao iniciar a session factory");
			throw new ExceptionInInitializerError(e);
		}
	}

	public static SessionFactory getSessionfactory() {
		return sessionFactory;
	}

}
