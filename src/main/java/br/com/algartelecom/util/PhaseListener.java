package br.com.algartelecom.util;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

import org.hibernate.Session;

public class PhaseListener implements javax.faces.event.PhaseListener {

	private static final long serialVersionUID = 1L;

	public void beforePhase(PhaseEvent phaseEvent) {
		if (phaseEvent.getPhaseId().equals(PhaseId.RESTORE_VIEW)) {
			System.out.println("Antes da fase: " + phaseEvent.getPhaseId());
			Session session = HibernateUtil.getSessionfactory().openSession();
			session.beginTransaction();
			FacesContextUtil.setRequestSession(session);
		}
	}

	public void afterPhase(PhaseEvent phaseEvent) {
		if (phaseEvent.getPhaseId().equals(PhaseId.RENDER_RESPONSE)) {
			System.out.println("Depois da fase: " + phaseEvent.getPhaseId());
			Session session = FacesContextUtil.getRequestSession();
			try {
				session.getTransaction().commit();
			} catch (Exception e) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().rollback();
				}
			} finally {
				session.close();
			}
		}
	}

	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}
