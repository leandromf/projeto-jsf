package br.com.algartelecom.support;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.algartelecom.model.dao.HibernateDAO;
import br.com.algartelecom.model.dao.InterfaceDAO;
import br.com.algartelecom.model.entity.Sexo;
import br.com.algartelecom.util.FacesContextUtil;

@ManagedBean(name = "bbSexo")
@RequestScoped
public class BbSexo implements Serializable {

	private static final long serialVersionUID = 1L;

	public List<Sexo> getSexos() {
		InterfaceDAO<Sexo> sexoDAO = new HibernateDAO<Sexo>(Sexo.class, FacesContextUtil.getRequestSession());
		return sexoDAO.getEntities();
	}

}
