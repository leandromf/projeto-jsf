package br.com.algartelecom.support;

import br.com.algartelecom.model.dao.HibernateDAO;
import br.com.algartelecom.model.dao.InterfaceDAO;
import br.com.algartelecom.model.entity.Estado;
import br.com.algartelecom.util.FacesContextUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "bbEstado")
@RequestScoped
public class BbEstado implements Serializable {

	private static final long serialVersionUID = 1L;

	public List<Estado> getEstados() {
		InterfaceDAO<Estado> estadoDAO = new HibernateDAO<Estado>(Estado.class, FacesContextUtil.getRequestSession());
		return estadoDAO.getEntities();
	}

}