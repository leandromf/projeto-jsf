package br.com.algartelecom.support;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.algartelecom.model.dao.HibernateDAO;
import br.com.algartelecom.model.dao.InterfaceDAO;
import br.com.algartelecom.model.entity.Cidade;
import br.com.algartelecom.util.FacesContextUtil;

@ManagedBean(name = "bbCidade")
@RequestScoped
public class BbCidade implements Serializable {

	private static final long serialVersionUID = 1L;

	public List<Cidade> getCidades() {
		InterfaceDAO<Cidade> cidadeDAO = new HibernateDAO<Cidade>(Cidade.class, FacesContextUtil.getRequestSession());
		return cidadeDAO.getEntities();
	}

}
