package br.com.algartelecom.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import br.com.algartelecom.model.dao.HibernateDAO;
import br.com.algartelecom.model.dao.InterfaceDAO;
import br.com.algartelecom.model.entity.Cidade;
import br.com.algartelecom.util.FacesContextUtil;

@ManagedBean(name = "mbCidade")
@RequestScoped
public class MbCidade implements Serializable {

	private static final long serialVersionUID = 1L;

	private Cidade cidade = new Cidade();
	private List<Cidade> cidades;

	public MbCidade() {

	}

	private InterfaceDAO<Cidade> cidadeDAO() {
		InterfaceDAO<Cidade> cidadeDAO = new HibernateDAO<Cidade>(Cidade.class, FacesContextUtil.getRequestSession());
		return cidadeDAO;
	}

	public String flushCidade() {
		cidade = new Cidade();
		return editCidade();
	}

	public String editCidade() {
		return "/restrict/cadastrarcidade.faces";
	}

	public String addCidade() {
		if (cidade.getIdCidade() == null || cidade.getIdCidade() == 0) {
			insertCidade();
		} else {
			updateCidade();
		}
		flushCidade();
		return null;
	}

	private void insertCidade() {
		cidadeDAO().save(cidade);
		FacesContext.getCurrentInstance().addMessage("formCidade:msg0",
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Inserção efetuada com sucesso", ""));
	}

	private void updateCidade() {
		cidadeDAO().update(cidade);
		FacesContext.getCurrentInstance().addMessage("formCidade:msg0",
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));
	}

	public void deleteCidade() {
		cidadeDAO().remove(cidade);
		FacesContext.getCurrentInstance().addMessage("formCidade:msg0",
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão efetuada com sucesso", ""));
	}

	public Cidade getCidade() {
		return cidade;
	}

	public List<Cidade> getCidades() {
		cidades = cidadeDAO().getEntities();
		return cidades;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

}
