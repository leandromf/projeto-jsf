package br.com.algartelecom.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.algartelecom.converter.ConverterSHA1;
import br.com.algartelecom.model.dao.HibernateDAO;
import br.com.algartelecom.model.dao.InterfaceDAO;
import br.com.algartelecom.model.entity.Endereco;
import br.com.algartelecom.model.entity.Pessoa;
import br.com.algartelecom.util.FacesContextUtil;

@ManagedBean(name = "mbPessoa")
@SessionScoped
public class MbPessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	private Pessoa pessoa = new Pessoa();
	private Endereco endereco = new Endereco();
	private List<Pessoa> pessoas;
	private List<Endereco> enderecos;
	private String confereSenha;

	public MbPessoa() {

	}

	private InterfaceDAO<Pessoa> pessoaDAO() {
		InterfaceDAO<Pessoa> pessoaDAO = new HibernateDAO<Pessoa>(Pessoa.class, FacesContextUtil.getRequestSession());
		return pessoaDAO;
	}

	private InterfaceDAO<Endereco> enderecoDAO() {
		InterfaceDAO<Endereco> enderecoDAO = new HibernateDAO<Endereco>(Endereco.class,
				FacesContextUtil.getRequestSession());
		return enderecoDAO;
	}

	public String flushPessoa() {
		pessoa = new Pessoa();
		endereco = new Endereco();
		return editPessoa();
	}

	public String editPessoa() {
		return "/restrict/cadastrarpessoa.faces";
	}

	public String addPessoa() {
		Date date = new Date();
		if (pessoa.getIdPessoa() == null || pessoa.getIdPessoa() == 0) {
			pessoa.setDataDeCadastro(date);
			insertPessoa();
		} else {
			updatePessoa();
		}
		return null;
	}

	private void insertPessoa() {
		pessoa.setSenha(ConverterSHA1.cipher(pessoa.getSenha()));
		if (pessoa.getSenha().equals(ConverterSHA1.cipher(confereSenha))) {
			pessoa.setPermissao("ROLE_ADMIN");
			pessoaDAO().save(pessoa);
			endereco.setPessoa(pessoa);
			enderecoDAO().save(endereco);
			FacesContext.getCurrentInstance().addMessage("formPessoa:msg0",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Inserção efetuada com sucesso", ""));
		} else {
			FacesContext.getCurrentInstance().addMessage("formPessoa:msg0",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "As senhas não conferem", ""));
		}
	}

	private void updatePessoa() {
		pessoaDAO().update(pessoa);
		enderecoDAO().update(endereco);
		System.out.println("Atualizando endereco para " + endereco);
		FacesContext.getCurrentInstance().addMessage("formPessoa:msg0",
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));
	}

	public void deletePessoa() {
		pessoaDAO().remove(pessoa);
		enderecoDAO().remove(endereco);
		FacesContext.getCurrentInstance().addMessage("formPessoa:msg0",
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão efetuada com sucesso", ""));
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public List<Pessoa> getPessoas() {
		pessoas = pessoaDAO().getEntities();
		return pessoas;
	}

	public List<Endereco> getEnderecos() {
		enderecos = enderecoDAO().getEntities();
		return enderecos;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public String getConfereSenha() {
		return confereSenha;
	}

	public void setConfereSenha(String confereSenha) {
		this.confereSenha = confereSenha;
	}

}
